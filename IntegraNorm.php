<!DOCTYPE html>
    <html>
        <head>
            <meta charset="UTF-8">
            <title>Portal de Integración</title>
		    <link rel="shortcut icon" href="img/automa.png">
            <meta charset="utf-8">
	        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
            <link rel="stylesheet" media="all" href="css/bootstrap-3.3.6.min.css">
            <link rel="stylesheet" type="text/css" href="http://10.191.135.124/sytleATP.css">
            <!-- <link rel="stylesheet" media="all" href="https://10.191.33.12/resources/stylesheets/bootstrap-theme-3.3.6.min.css">-->
            <!-- -->
	        <style type="text/css">
                @-webkit-viewport{width:device-width;}
                @-moz-viewport{width:device-width;}
                @-ms-viewport{width:device-width;}
                @-o-viewport{width:device-width;}
                @viewport{width:device-width;}
            </style>
            <link rel="stylesheet" media="all" href="https://10.191.33.12/resources/stylesheets/datatables-1.10.11.min.css">
            <link rel="stylesheet" media="all" href="https://10.191.33.12/oo/application.css">
            <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
            <script type="text/javascript" src="js/jquery.js"></script>
            <script type="text/javascript" src="js/jquery-3.0.0.js"></script>
            <script>
                function realizaProceso(valorCaja1)
                {
                    var parametros = {
                        "valorCaja1" : valorCaja1
                    };
                    $.ajax({
                        data:  parametros,
                        url:   'include/checkFolder.php',
                        type:  'post',
                        beforeSend: function () {
                            $("#resultado").html("Procesando, espere por favor...");
                        },
                        success:  function (response) {
                            $("#resultado").html(response);
                        }
                    });
                }
                function realizaProceso2(valorCaja2){
                    var parametros2 = {
                        "valorCaja2" : valorCaja2
                    };
                    $.ajax({
                        data:  parametros2,
                        url:   'include/rename.php',
                        type:  'post',
                        beforeSend: function () {
                            $("#resultado2").html("Procesando, espere por favor...");
                        },
                        success:  function (response) {
                            $("#resultado2").html(response);
                        }
                    });
                }              
                function mostrar(id) 
                {
                    if (id == "White") 
                    {
                        $("#White").show();
                        $("#NueProyec").hide();
                        $("#EditProyec").hide();
                    }
                    if (id == "NueProyec") 
                    {
                        $("#NueProyec").show();
                        $("#White").hide();
                        $("#EditProyec").hide();
                    }
                    if (id == "EditProyec") 
                    {
                        $("#NueProyec").hide();
                        $("#White").hide();
                        $("#EditProyec").show();
                    }
                }
                $(document).ready(function()
                {
                    $("#txtbusca").keyup(function()
                    {
                        var parametros="txtbusca="+$(this).val()
                        $.ajax({
                            data:  parametros,
                            url:   'include/salida.php',
                            type:  'post',
                            beforeSend: function () { },
                            success:  function (response) 
                            {
                                $(".salida").html(response);
                            },
                            error:function()
                            {
                                alert("error")
                            }
                        });
                    })
                })
            </script>
        </head>
        <body>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <span class="sr-only">Navegación</span>
                        <a class="navbar-brand" href="http://localhost/FlujosNormatividad/index.php">
                            <img src="img/telcel.png" alt="Icono">
                        </a>
                        <p class="navbar-text">Portal Integral de flujos</p>
                    </div>
                </div>
            </nav>
            <div class="col-lg-12">
                <div class="row"> 
                    <div class="col-sm-7">
                        <div class="panel panel-default" id="element-flows" style="display: block;">
                            <div class="panel-heading">Administración de proyectos.</div>
                            <div class="panel-body">
                                <div class="list-group" id="flows-list" style="display: block;">
                                    <form action="IntegraNorm.php" method="post">
                                        Selecciona una opción: 
                                        <select id="status" name="status" onChange="mostrar(this.value);">                        
                                            <option value="White">------------------------</option>
                                            <option value="NueProyec">Crear Nuevo Proyecto</option>                    
                                            <option value="EditProyec">Editar Proyecto Existente</option>
                                        </select>
                                    </form>                    
                                    <div id="NueProyec" style="display: none;">
                                        <br>
                                        <input type="text" name="caja_texto" id="valor1" value=""/>
                                        <input type="button" href="javascript:;" onclick="realizaProceso($('#valor1').val());return false;" value="Nuevo Proyecto"/>
                                        <br/>
                                        <span id="resultado"></span>
                                        <div class="activity-spinner" id="following-activity" style="display: none;">
                                            <img alt="Redirigiendo..." src="img/spinner.gif">
                                        </div>
                                        <br>
                                        <span id="resultado"></span>                   
                                    </div>                        
                                    <div id="EditProyec" style="display: none;">
                                        <br>
                                        <input type="text" name="caja_texto2" id="valor2" value=""/>
                                        <input type="button" href="javascript:;" onclick="realizaProceso2($('#valor2').val());return false;" value="Editar Proyecto"/>
                                        <span>Digita el ID del proyecto.</span>
                                        <br/>
                                        <span id="resultado2"></span>
                                        <div class="activity-spinner" id="following-activity" style="display: none;">
                                            <img alt="Redirigiendo..." src="img/spinner.gif">
                                        </div>
                                        <br>
                                        <span id="resultado2"></span>                   
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="panel panel-default panel-success" id="element-following" style="display: block;">
                            <div class="panel-heading">Lista de proyectos.</div>
                            <div class="panel-body">
                                <div class="list-group" id="following-list" style="display: none;"></div>
                                <div class="activity-spinner" id="following-activity" style="display: none;">
                                    <img alt="Procesando..." src="img/spinner.gif">
                                </div>                   
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" id="txtbusca" placeholder="Buscar" aria-label="Buscar" aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon2"></span> <!-- Entre la etiqueta span se puede poner alguna leyenda-->
                                    </div>
                                    <br><br><br>
                                </div>
                                <div class="salida"></div>                   
                            
                                </div>
                        </div>
                    </div>
                </div>   
            </div>
        </body>
    </html>